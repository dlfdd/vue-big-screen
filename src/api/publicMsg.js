import request from '../utils/request'

//加载月份和数量
export function get_mouth_count() {
    return request({
        url: '/commonmsg/mouth_count',
        method: 'get'
    })
}