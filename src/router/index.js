import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [{
    path: '/',
    name: 'index',
    component: () => import('../views/demo/index.vue')
},

    {
        path: '/publicMsg',
        name: 'publicMsg',
        component: () => import('../views/publicMsg/index.vue')
    }
]
const router = new VueRouter({
    mode: "history",
    routes
})

export default router