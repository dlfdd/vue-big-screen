const path = require('path')
const resolve = dir => {
  return path.join(__dirname, dir)
}
module.exports = {
  lintOnSave: false, //关闭eslint
  publicPath: './',
  devServer: {
    proxy: {
      '/dev-api': {
        // target: 'http://10.23.112.163:8090/',
        target: 'http://127.0.0.1:9999/',
        changeOrigin: true,
        pathRewrite: {
          '^/dev-api': ''
        }
      }

    }

  },
  chainWebpack: config => {
    config.resolve.alias
      .set('_c', resolve('src/components')) // key,value自行定义，比如.set('@@', resolve('src/components'))
  },
}
// // vue.config.js
// module.exports = {
//
// }
